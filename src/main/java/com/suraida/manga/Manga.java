/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suraida.manga;

/**
 *
 * @author sulaiman
 */
public class Manga {
    protected String name;
    protected String writer;
    protected String publisher;
    protected String type;
    
    public Manga(String name , String writer , String publisher , String type){
        this.name = name;
        this.writer = writer;
        this.publisher = publisher;
        this.type = type;
    }
    
    //สร้าง Method ชื่อ recommend เพื่อแสดงค่าที่รับมาชื่อหนังสือ , อาจารย์ที่เขียน , สำนักพิมพ์ , ประเภทหนังสือ
    public void recommend(){
        System.out.println("This is manga : " + name + " , This manga write by: " + writer + " And public by: " + publisher);
        System.out.println("This manga about: " + type);
    }
    
    //สร้าง Method เพื่อไว้แบ่งหนังสือ
    public void blank(){
       System.out.println("---------------------------------------------------- Detail ----------------------------------------------------------");
    }
    
    //สร้าง Method ไว้จบการแบ่งหนังสือ
    public void finish(){
        System.out.println("------------------------------------------------ Finish --------------------------------------------------------------");
        System.out.println();
    }
}
