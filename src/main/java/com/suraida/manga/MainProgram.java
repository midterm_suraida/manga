/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suraida.manga;

/**
 *
 * @author sulaiman
 */
public class MainProgram {
    public static void main(String[] args) {
        Manga1 only = new Manga1("Romantic");
        only.blank();
        only.recommend();
        only.finish();
        
        Manga2 reborn = new Manga2("Mafia");
        reborn.blank();
        reborn.recommend();
        reborn.finish();
        
        Manga3 air = new Manga3("Adventure");
        air.blank();
        air.recommend();
        air.finish();
        
        Manga4 exe = new Manga4("Adventure");
        exe.blank();
        exe.recommend();
        exe.finish();
    }
}
